#!/usr/bin/env python
# -*- coding: utf-8 -*-

from image_report import Report

report = Report('data', ['crop', 'mask', 'seg'], coronal_slices=[110, 130, 150])
report.save('report')
