#!/usr/bin/env python
# -*- coding: utf-8 -*-


from image_report import Montage

image_dirs = ['pics/image_step1', 'pics/image_step2', 'pics/image_step3']
slice_indices = [110, 120, 130, 140, 150]

montage = Montage(image_dirs, slice_indices, 'tmp.png')
montage.save()
