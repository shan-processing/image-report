# -*- coding: utf-8 -*-

import os
from glob import glob
from itertools import chain


class Montage:
    """Stitch a matrix of pictures together using imagemagick `montage`

    By default, a row is image modes (usually processing steps), and a column is
    the slices to show. It also provides an option to transpose this matrix.

    Attributes:
        images (list of list): A matrix of images to montage. A row is image
            modes and a column is slices

    """
    def __init__(self, image_dirs, slice_indices):
        """Initialize
        
        A slice in a image directory should be names as
        ${image_dir}/*${ind}.${ext}

        Args:
            image_dirs (list of str): Each element should contains all the
                slices of a specific mode of the 3D image. A slice picture
                should have the slice index as a suffix before the extention
            slice_indices (list of str): The slices to show

        Raises:
            RuntimeError: Image slice does not exist
            RuntimeError: Image slice is not unique

        """
        self.images = list()
        for ind in slice_indices:
            images = list()
            for dir in image_dirs:
                pattern = os.path.join(dir, '*'+str(ind)+'.*')
                image = glob(pattern)
                if len(image) == 0:
                    raise RuntimeError('%s does not exist' % pattern)
                elif len(image) > 1:
                    raise RuntimeError('%s is not unique' % pattern)
                images.append(image[0])
            self.images.append(images)
    
    def save(self, output_path, transpose=False):
        """Stitch pictures and save

        Args:
            output_path (str): The output path of the montage
            transpose (bool): Transpose the image matrix so row is slices and
                column is modes

        """
        if transpose:
            images = list(zip(*self.images))
        else:
            images = self.images
        tile = '%dx%d' % (len(images[0]), len(images))
        images = list(chain(*images))
        command = 'montage %s -tile %s -geometry +0+0 %s'
        command = command % (' '.join(images), tile, output_path)
        os.system(command)
