# -*- coding: utf-8 -*-

import os
from glob import glob
from collections import OrderedDict

from .montage import Montage


class Report:
    """Image processing report 

    The images should be structured into BIDS and already converted to png
    slices. A converted is structured as ([*] means optional):

    PROC_DIR/
        - sub-SUBJ_ses-SESS_PROTOCAL_PROC/
            - [axial/]
                - SLICE-IND.png
            - [coronal/]
            - [sagittal/]

    Only support "anat" sub-folder right now.

    Attributes:
        proc_dir (str): The directory of the processing

    """
    def __init__(self, proc_dir, suffixes,
                 axial_slices=[], coronal_slices=[], sagittal_slices=[]):
        """Initialize

        Args:
            proc_dir (str): The processing directory
            axial_slices (list of str): The axial slices to show
            coronal_slices (list of str): The axial slices to show
            sagittal_slices (list of str): The axial slices to show

        """
        slice_indices = OrderedDict()
        if axial_slices:
            slice_indices['axial'] = axial_slices
        if coronal_slices:
            slice_indices['coronal'] = coronal_slices
        if sagittal_slices:
            slice_indices['sagittal'] = sagittal_slices

        self.montages = OrderedDict()
        for sub_dir in sorted(glob(os.path.join(proc_dir, 'sub-*'))):
            sub = os.path.basename(sub_dir)
            self.montages[sub] = OrderedDict()
            for ses_dir in sorted(glob(os.path.join(sub_dir, 'ses-*'))):
                ses = os.path.basename(ses_dir)
                ses_dir = os.path.join(ses_dir, 'anat')
                self.montages[sub][ses] = OrderedDict()
                image_dirs = list()
                for suffix in suffixes:
                    image_dir = sorted(glob(os.path.join(ses_dir, '*'+suffix)))
                    if len(image_dir) == 0:
                        raise RuntimeError('%s does not exist.' % suffix)
                    else:
                        image_dirs += image_dir
                for v, ind in slice_indices.items():
                    dirs = [os.path.join(d, v) for d in image_dirs]
                    self.montages[sub][ses][v] = Montage(dirs, ind) 

    def save(self, output_dir, transpose=False):
        """Save reprot to .md and convert to .html

        Args:
            output_dir (str): The output directory of the report
            transpose (bool): Montage row is slice indices, column is modes

        """
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        md_report = os.path.join(output_dir, 'report.md')
        html_report = os.path.join(output_dir, 'report.html')
        md_pic_dir = os.path.join(output_dir, 'pics')
        if not os.path.isdir(md_pic_dir):
            os.makedirs(md_pic_dir)
        
        md = open(md_report, 'w')
        for sub, ses_montages in self.montages.items():
            md.write('## %s\n' % sub)
            for ses, view_montages in ses_montages.items():
                md.write('### %s\n' % ses)
                for view, montage in view_montages.items():
                    montage_filename = '_'.join((sub, ses, view)) + '.png'
                    montage_path = os.path.join(md_pic_dir, montage_filename)
                    montage.save(montage_path, transpose=transpose)
                    relative_path = os.path.join('pics', montage_filename)
                    md.write('![%s](%s)\n\n'%(montage_filename, relative_path))
        md.close()
        os.system('pandoc -fmarkdown-implicit_figures -o %s %s' \
                  % (html_report, md_report))
