#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

desc = 'Generate report of image processing'
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('-d', '--proc-dir', required=True,
                    help='The processing directory in BIDS format')
parser.add_argument('-s', '--suffix', nargs='+', required=True,
                    help='The suffix of directory of pics')
parser.add_argument('-as', '--axial-slice', nargs='+', type=int,
                    help='The index of the axial slice to show')
parser.add_argument('-cs', '--coronal-slice', nargs='+', type=str,
                    help='The index of the coronal slice to show')
parser.add_argument('-ss', '--sagittal-slice', nargs='+', type=int,
                    help='The index of the sagitttal slice to show')
parser.add_argument('-t', '--transpose', default=False, action='store_true',
                    help='Transpose the montage image')
args = parser.parse_args()


from image_report import Report


report = Report(args.proc_dir, args.suffix,
                axial_slices=args.axial_slice,
                coronal_slices=args.coronal_slice,
                sagittal_slices=args.sagittal_slice)
report.save(os.path.join(args.proc_dir, 'report'), args.transpose)
