#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Generate volume table')
parser.add_argument('-i', '--input-dir', nargs='+', help='The BIDS path')
parser.add_argument('-s', '--suffix', default=None,
                    help='The suffix of the volume file')
parser.add_argument('-b', '--exclude-bad', action='store_true', default=False,
                    help='Exclude bad segmentation in bad.tsv file')
parser.add_argument('-a', '--exclude-nan-age',
                    action='store_true', default=False,
                    help='Exclude subjects with NaN ages')
parser.add_argument('-l', '--min-num-sessions', type=int, default=0,
                    help='Select subject with as least this number of sessions')
parser.add_argument('-o', '--output-file', help='The output file path')
parser.add_argument('-d', '--diagnosis', nargs='+',
                    help='Select subject with this diagnosis')
parser.add_argument('-c', '--cognition', nargs='+', help='Cognition scores')
args = parser.parse_args()

import os
from glob import glob
import pandas as pd
import numpy as np


all_volumes = list()

for input_dir in args.input_dir:
    demography_tsv = os.path.join(input_dir, 'participants.tsv')
    icv_tsv = os.path.join(input_dir, 'phenotypes', 'icv.tsv')
    demography_df = pd.read_csv(demography_tsv, sep='\t', index_col=0,
                                dtype={'diagnosis':str})
    icv_df = pd.read_csv(icv_tsv, sep='\t', index_col=0)
    participant_ids = list()
    participant_volumes = list()

    for subj_dir in sorted(glob(os.path.join(input_dir, 'sub-*'))):
        subj = os.path.basename(subj_dir)

        try:
            diagnosis = demography_df.at[subj, 'diagnosis']
        except KeyError:
            continue 

        if diagnosis not in args.diagnosis:
            continue

        try:
            icv = icv_df.at[subj, 'icv']
        except KeyError:
            continue

        sessions_tsv = os.path.join(subj_dir, 'sessions.tsv')
        if not os.path.isfile(sessions_tsv):
            continue
        sessions_df = pd.read_csv(sessions_tsv, sep='\t', index_col=0)
        if 'birthday' in demography_df.columns:
            birthday = demography_df.at[subj, 'birthday']
        else:
            birthday = float('nan')

        try:
            sex = demography_df.at[subj, 'sex']
        except KeyError:
            continue

        session_volumes = list()

        for sess_dir in sorted(glob(os.path.join(subj_dir, 'ses-*'))):
            anat_dir = os.path.join(sess_dir, 'anat')
            sess = os.path.basename(sess_dir)
            volume_tsv = glob(os.path.join(anat_dir, '*%s.tsv'%args.suffix))
            if len(volume_tsv) == 0:
                continue
            volume_tsv = volume_tsv[0]
            volume_df = pd.read_csv(volume_tsv, sep='\t')
            volume_df.index = [sess]
            volume_df.index.name = 'session_id'
            try:
                acq_time = sessions_df.at[sess, 'acq_time']
            except KeyError:
                continue

            if pd.notna(birthday) and pd.notna(acq_time):
                birthday = pd.to_datetime(birthday)
                acq_time = pd.to_datetime(acq_time)
                age = round((acq_time - birthday).days / 365, 1)
            elif 'age' in sessions_df.columns:
                age = sessions_df.at[sess, 'age']
            else:
                age = float('nan')

            volume_df.insert(loc=0, column='age', value=[age])
            volume_df.insert(loc=1, column='sex', value=[sex])
            volume_df.insert(loc=2, column='diagnosis', value=[diagnosis])
            volume_df.insert(loc=3, column='icv', value=[icv])
            
            for cog in args.cognition:
                cog_path = glob(os.path.join(sess_dir, 'cog', '*%s.tsv'%cog))
                if len(cog_path) == 0:
                    continue
                cog_path = cog_path[0]
                cog_df = pd.read_csv(cog_path, sep='\t')
                cog_df.index = [sess]
                volume_df = volume_df.join(cog_df)

            session_volumes.append(volume_df)

        if len(session_volumes) > 0:
            session_vol_df = pd.concat(session_volumes, axis=0)
            participant_ids.append(subj)
            participant_volumes.append(session_vol_df)

    if len(participant_volumes) == 0:
        continue
    participant_vol_df = pd.concat(participant_volumes, axis=0,
                                   keys=participant_ids,
                                   names=['participant_id'])
    participant_vol_df = participant_vol_df.reset_index(level=[0, 1])

    if args.exclude_bad:
        bad_tsv = os.path.join(input_dir, 'bad.tsv')
        bad_df = pd.read_csv(bad_tsv, sep='\t')
        data = participant_vol_df['participant_id'] \
             + participant_vol_df['session_id']
        bad = bad_df['participant_id'] + bad_df['session_id']
        ind = ~data.isin(bad)
        participant_vol_df = participant_vol_df[ind.values]

    all_volumes.append(participant_vol_df)

df = pd.concat(all_volumes, axis=0)
df = df.sort_values(by=['participant_id', 'age'])

if args.exclude_nan_age:
    ind = ~df['age'].isnull()
    df = df[ind]

if args.min_num_sessions > 1:
    counts = df.groupby(['participant_id']).size()
    ind = counts >= args.min_num_sessions
    subjs = counts[ind].index.values
    df = df[df['participant_id'].isin(subjs)]

df.to_csv(args.output_file, index=False)
