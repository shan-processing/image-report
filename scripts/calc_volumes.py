#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Calculate volumes')
parser.add_argument('datadir', help='Diretory in BIDS')
parser.add_argument('-s', '--suffix', default=None,
                    help='The suffix of the segmentation')
parser.add_argument('-a', '--not-anat', default=False, action='store_true')
args = parser.parse_args()

import os
from glob import glob
import nibabel as nib
import numpy as np
import pandas as pd
from collections import defaultdict


for sub in sorted(glob(os.path.join(args.datadir, 'sub-*'))):
    pattern = os.path.join(sub, 'ses-*')
    if not args.not_anat:
        pattern = os.path.join(pattern, 'anat')
    for ses in sorted(glob(pattern)):
        volumes = defaultdict(list)
        seg_path = glob(os.path.join(ses, '*'+args.suffix+'.nii*'))[0]
        seg_obj = nib.load(seg_path)
        seg = seg_obj.get_data()
        scale = np.prod(seg_obj.header.get_zooms())
        labels = np.unique(seg)
        for l in labels:
            volume = np.sum(seg == l).astype(float)
            volume = round(volume * scale)
            volumes[int(l)].append(volume)
        df = pd.DataFrame(volumes)
        output_path = seg_path.replace('.nii', '').replace('.gz', '')
        output_path += '_volumes.tsv'
        df.to_csv(output_path, sep='\t', index=False)
